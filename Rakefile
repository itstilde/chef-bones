require 'chef'
require 'foodcritic'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'bundler/setup'
require 'mixlib/shellout'

task default: [:list]

desc 'Lists all the tasks.'
task :list do
  puts "Tasks: \n- #{Rake::Task.tasks.join("\n- ")}"
end

desc 'Run all tasks'
task all: [
  :clean,
  :test,
  :mock,
  :readme
]

desc 'Runs test tasks'
task test: [
  :food,
  :rubocop,
  :mock,
  :spec,
  :kitchen
]

desc 'Initalizes cookbook metadata cookbook from the config.json file & installs pre-deps'
task :build do
  abort 'No config.json file found.' unless ::File.exist?('./config.json')
  config = JSON.parse(::IO.read('./config.json'))
  abort 'Please enter a cookbook name.' unless config['name'] != 'CHANGETHIS'
  puts config
end

desc 'Creates README & Other documentation files'
task :readme do
  Mixlib::ShellOut.new('bundle exec knife cookbook doc -t README.md.erb .').run_command
end

desc 'Creates mock JSON attr file for using in :spec'
task :mock do
  require 'knife_cookbook_doc/attributes_model'
  require 'json'
  def flat_keys_to_nested(hash)
    hash.each_with_object({}) do |(key, value), all|
      key_parts = key.split('.').map!(&:to_sym)
      leaf = key_parts[0...-1].inject(all) { |acc, elem| acc[elem] ||= {} }
      leaf[key_parts.last] = value
    end
  end

  # Generate metadata.json
  # _metadata_cmd = Mixlib::ShellOut.new('knife cookbook metadata from file ./metadata.rb').run_command
  # puts _metadata_cmd.stdout
  # _metadata = JSON.parse(::IO.read('./metadata.json'))
  attributes = {}
  Dir['./attributes/*.rb'].sort.each do |attribute_filename|
    model = KnifeCookbookDoc::AttributesModel.new(attribute_filename)
    model.attributes.each do |attr|
      short_name = attr[0].strip.gsub(/node\['/, '').gsub(/'\]\['/, '.').gsub(/'\]$/, '')
      # attributes[short_name] = attr[2].gsub(/#{@metadata['name']}/, '')
      attributes[short_name] = attr[2]
    end
  end
  attributes = flat_keys_to_nested attributes
  ::IO.write('test/attributes.json', attributes.to_json)
end

desc 'runs rspec'
task :spec do
  RSpec::Core::RakeTask.new
end

desc 'Pedantic style checker'
task :rubocop do
  RuboCop::RakeTask.new do |t|
    t.formatters = ['progress']
  end
end

desc 'Mmm food!'
task :food do
  FoodCritic::Rake::LintTask.new do |t|
    t.options = { fail_tags: ['all'] }
  end
end

desc 'Runs kitchen tests'
task :kitchen do
  begin
    require 'kitchen/rake_tasks'
    Kitchen::RakeTasks.new
  rescue LoadError
    puts '>>>>> Kitchen gem not loaded, omitting tasks.' unless ENV['CI']
  end
end

desc 'Cleans up the syntax'
task :clean do
  Mixlib::ShellOut.new("find . -name \"*.rb\" -exec ruby-beautify '{}' ';' > /dev/null 2>&1").run_command
  %w(Rakefile Gemfile Berksfile Thorfile).each do |clean_me|
    Mixlib::ShellOut.new("bundle exec ruby-beautify #{clean_me} && sed -i 's/ *$//' #{clean_me} > /dev/null 2>&1").run_command
  end
  # Removes trailing white spaces if there's any left over
  Mixlib::ShellOut.new("find . -name \"*.rb\" -exec sed -i 's/ *$//' '{}' ';' > /dev/null 2>&1").run_command
end
