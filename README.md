# Chef Bones
Does some voodoo magic and makes you a nice cookbook.

## Usage

*1) Clone the cookbook repo:*

`git clone git@bitbucket.org:jacob_mackenzie/chef-bones.git`

*2) Make a configuration file to add default cookbook data*

`cp config.json.example config.json`

*3) The Rake file expects the following structure in* `config.json`

```
{
  "name" : "yourname",
  "author" : "your name",
  "email" : "your@email.com",
  "description" : "A default description"
}
```

At minimum you need to change the `name` value.

These values will rewrite values in `metadata.rb` and `README.md.erb`

*4) Execute* `rake build`

## TODO

* Write code in :build that replaces all the strings with info
* Write code in :build to do gem/berks stuff
* generate `config.json` data from user in `rake configure`

## Contributing

Fork it and start breaking it for me so we can fix it together

## License

MIT License

Copyright (c) 2016 its~

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
